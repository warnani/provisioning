const minimist = require('minimist')
const chalk = require('chalk')
const Instance = require('../src/Instance/Instance')

const args = minimist(process.argv.slice(2), {
    string: [ 'instance_id', 'action', 'name', 'type' ],
    alias: {
        'i' : 'instance_id',
        'a' : 'action',
        'n' : 'name',
        't' : 'type'
    }
})

const action = args.action

if (undefined === action) {
    console.error(chalk.red(`Undefined action`))

    process.exit(1)
}

const catchError = (error) => {
    console.error(chalk.red(error.message))

    process.exit(1)
}

switch (action) {
    case 'list':
        Instance.list().then((servers) => {
            console.table(servers)
        }).catch(error => catchError(error))
        break;
    case 'create':
        Instance.create(args.name, args.type).then((servers) => {
            console.table(servers)
        }).catch(error => catchError(error))
        break;
    case 'start':
        Instance.start(args.instance_id).then((responseMessage) => {
            console.log(chalk.green(responseMessage))
        }).catch(error => catchError(error))
        break
    case 'reboot':
        Instance.reboot(args.instance_id).then((responseMessage) => {
            console.log(chalk.green(responseMessage))
        }).catch(error => catchError(error))
        break
    case 'pause':
        Instance.pause(args.instance_id).then((responseMessage) => {
            console.log(chalk.green(responseMessage))
        }).catch(error => catchError(error))
        break
    case 'shutdown':
        Instance.shutdown(args.instance_id).then((responseMessage) => {
            console.log(chalk.green(responseMessage))
        }).catch(error => catchError(error))
        break
    case 'terminate':
        Instance.terminate(args.instance_id).then((responseMessage) => {
            console.log(chalk.green(responseMessage))
        }).catch(error => catchError(error))
        break
    case 'delete':
        Instance.delete(args.instance_id).then((responseMessage) => {
            console.log(chalk.green(responseMessage))
        }).catch(error => catchError(error))
        break
    case 'status':
        Instance.status(args.instance_id).then((responseMessage) => {
            console.log(chalk.green(`"${args.instance_id}" is currently "${responseMessage}"`))
        }).catch(error => catchError(error))
        break
    default:
        console.error(chalk.red(`Unsupported action "${action}"`))
}