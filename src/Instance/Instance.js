const parameters = require('../../config/parameters')
const fetch = require('node-fetch')
const _ = require('lodash')

const baseUrl = parameters.base_url + '/' + parameters.zone + '/servers'
const requestParameters = {
    headers: new fetch.Headers()
}

requestParameters.headers.append('X-Auth-Token', parameters.secret)

const doAction = (instanceId, action) => {
    return new Promise((resolve, reject) => {
        if (undefined === instanceId) {
            reject(new Error(`Undefined instanceId`))
        }

        const url = baseUrl + '/' + instanceId + '/action'
        const params = requestParameters

        params.method = 'POST'
        params.body = JSON.stringify({
            "action": action
        })
        params.headers.append('Content-Type', 'application/json')


        return fetch(url, params).then((response) => {
            if (202 === response.status) {
                resolve(`Done start action "${action}" on instance "${instanceId}"`)
            }

            response.text().then((content) => {
                console.log(content)
                reject(new Error(`Unable to start "${instanceId}" instance. Status code ${response.status}`))
            })
        })
    })
}

module.exports = {
    list: () => {
        return new Promise((resolve, reject) => {
            fetch(baseUrl, requestParameters).then((response) => {
                if (200 !== response.status) {
                    reject(new Error(`Unable to list servers error status "${response.status}"`))
                }
                response.text().then((content) => {
                    const servers = JSON.parse(content).servers

                    const hRServers = []

                    if(null !== servers) {
                        _.each(servers, (server) => {
                            hRServers.push({
                                id: server.id,
                                name: server.name,
                                ip: server.public_ip.address,
                                state: server.state
                            })
                        })
                    }

                    resolve(hRServers)
                })
            })
        })
    },

    create: (name, type) => {
        return new Promise((resolve, reject) => {
            if (undefined === name) {
                reject(new Error(`Undefined server name`))
            }

            if (undefined === type) {
                type = parameters.default_type
            }

            const options = requestParameters
            options.method = 'POST'
            options.body = JSON.stringify({
                name,
                commercial_type: type,
                organization: parameters.organization,
                image: parameters.default_image
            })
            options.headers.append('Content-Type', 'application/json')

            fetch(baseUrl, options).then(response => {
                if (201 !== response.status) {
                    response.text().then((content) => {
                        console.log(content)
                        reject(new Error(`Unable to create instance "${name}" with type "${type}"`))
                    })
                }

                response.text().then((content) => {
                    const server = JSON.parse(content).server
                    module.exports.start(server.id).then(() => {
                        resolve(server.id)
                    })
                })
            })
        })
    },

    start: (instanceId) => {
        return doAction(instanceId, 'poweron')
    },

    reboot: (instanceId) => {
        return doAction(instanceId, 'reboot')
    },

    pause: (instanceId) => {
        return doAction(instanceId, 'stop_in_place')
    },

    shutdown: (instanceId) => {
        return doAction(instanceId, 'poweroff')
    },

    terminate: (instanceId) => {
        return doAction(instanceId, 'terminate')
    },

    delete: (instanceId) => {
        return new Promise((resolve, reject) => {
            if (undefined === instanceId) {
                reject(new Error(`Undefined instanceId`))
            }

            const url = baseUrl + '/' + instanceId
            let params = requestParameters

            params.method = 'DELETE'
            params.headers.append('Content-Type', 'application/json')

            fetch(url, parameters).then(response => {
                if (200 !== response.status) {
                    return response.text().then(content => {
                        console.log(content)
                        reject(new Error(`Unable to retrieve status of "${instanceId}" instance. Status was "${response.status}"`))
                    })
                }

                resolve('done')
            })
        })
    },

    status: (instanceId) => {
        return new Promise((resolve, reject) => {
            if (undefined === instanceId) {
                reject(new Error(`Undefined instanceId`))
            }

            const url = baseUrl + '/' + instanceId

            fetch(url, requestParameters).then(response => {
                if (200 !== response.status) {
                    reject(new Error(`Unable to retrieve status of "${instanceId}" instance. Status was "${response.status}"`))
                }

                response.text().then((content) => {
                    resolve(JSON.parse(content).server.state)
                })
            })
        })
    }
}