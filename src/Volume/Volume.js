const parameters = require('../../config/parameters')
const fetch = require('node-fetch')
const _ = require('lodash')
const uuid = require('uuid')


const baseUrl = parameters.base_url+'/'+parameters.zone+'/volumes'
const requestParameters = {
    headers: new fetch.Headers()
}

requestParameters.headers.append('X-Auth-Token', parameters.secret)

module.exports = {
    create: (type, size) => {
        return new Promise((resolve, reject) => {
            if (undefined === type) {
                type = 'l_ssd'
            }

            if (undefined === size) {
                size = 21474836480
            }

            const options = requestParameters

            const name = uuid.v4()

            options.method = 'POST'
            options.headers.append('Content-Type', 'application/json')
            options.body = JSON.stringify({
                volume_type: type,
                size,
                organization: parameters.organization,
                name
            })

            fetch(baseUrl, options).then((response) => {
                response.text().then(content => {
                    if(201 !== response.status) {
                        console.log(content)

                        reject(new Error(`Unable to create volume`))
                    }

                    resolve(JSON.parse(content).volume)
                })


            })
        })
    },

    delete: (id) => {

    }
}